<?php

use yii\db\Migration;

/**
 * Class m200414_023059_restaurant
 */
class m200414_023059_restaurant extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('restaurant', [
            'id' => $this->primaryKey(),
            'name' => $this->string(60)->null(),
            'small_description' => $this->string(255)->null(),
            'description' => $this->text()->notNull(),
            'main_photo' => $this->text()->null(),
            'status' => $this->integer(11)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('restaurant');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023059_restaurant cannot be reverted.\n";

        return false;
    }
    */
}
