<?php

use yii\db\Migration;

/**
 * Class m200414_023217_restaurant_location
 */
class m200414_023217_restaurant_location extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('restaurant_location', [
            'id' => $this->primaryKey(),
            'restaurant_id' => $this->integer(11)->notNull(),
            'address' => $this->text()->notNull(),
            'phone' => $this->text()->notNull(),
            'status' => $this->integer(11)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('restaurant_location');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023217_restaurant_location cannot be reverted.\n";

        return false;
    }
    */
}
