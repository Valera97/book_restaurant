<?php

use yii\db\Migration;

/**
 * Class m200414_023321_restaurant_menu_dishes
 */
class m200414_023321_restaurant_menu_dishes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('restaurant_menu_dishes', [
            'id' => $this->primaryKey(),
            'restaurant_id' => $this->integer(11)->notNull(),
            'cat_id' => $this->integer(11)->notNull(),
            'name' => $this->string(62)->notNull(),
            'description' => $this->string(255)->notNull(),
            'price' => $this->float()->notNull(),
            'main_photo' => $this->text()->Null(),
            'status' => $this->integer(11)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('restaurant_menu_dishes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023321_restaurant_menu_dishes cannot be reverted.\n";

        return false;
    }
    */
}
