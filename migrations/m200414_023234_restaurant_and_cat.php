<?php

use yii\db\Migration;

/**
 * Class m200414_023234_restaurant_and_cat
 */
class m200414_023234_restaurant_and_cat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('restaurant_and_cat', [
            'id' => $this->primaryKey(),
            'restaurant_id' => $this->integer(11)->notNull(),
            'cat_id' => $this->integer(11)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('restaurant_and_cat');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023234_restaurant_and_cat cannot be reverted.\n";

        return false;
    }
    */
}
