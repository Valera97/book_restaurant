<?php

use yii\db\Migration;

/**
 * Class m200414_023250_order_dishes
 */
class m200414_023250_order_dishes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_dishes', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->notNull(),
            'dishes_id' => $this->integer(11)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_dishes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023250_order_dishes cannot be reverted.\n";

        return false;
    }
    */
}
