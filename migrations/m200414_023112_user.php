<?php

use yii\db\Migration;

/**
 * Class m200414_023112_user
 */
class m200414_023112_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'restaurant_id' => $this->integer(11)->null(),
            'name' => $this->string(62)->notNull(),
            'lastname' => $this->string(62)->notNull(),
            'login' => $this->string(62)->notNull(),
            'password' => $this->text()->notNull(),
            'email' => $this->string(120)->notNull(),
            'access' => $this->integer(11)->notNull(),
            'phone' => $this->string(120)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023112_user cannot be reverted.\n";

        return false;
    }
    */
}
