<?php

use yii\db\Migration;

/**
 * Class m200414_023149_paypal
 */
class m200414_023149_paypal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('paypal', [
            'id' => $this->primaryKey(),
            'paypal_order_id' => $this->string(127)->notNull(),
            'paypal_status' => $this->string(127)->notNull(),
            'paypal_amount' => $this->float()->notNull(),
            'order_amount' => $this->float()->notNull(),
            'order_id' => $this->integer(11)->null(),
            'hash' => $this->string(64)->notNull(),
            'paypal_payer_id' => $this->string(127)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('paypal');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023149_paypal cannot be reverted.\n";

        return false;
    }
    */
}
