<?php

use yii\db\Migration;

/**
 * Class m200414_023225_restaurant_cat
 */
class m200414_023225_restaurant_cat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('restaurant_cat', [
            'id' => $this->primaryKey(),
            'name' => $this->string(62)->notNull(),
            'parent_id' => $this->integer(11)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('restaurant_cat');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023225_restaurant_cat cannot be reverted.\n";

        return false;
    }
    */
}
