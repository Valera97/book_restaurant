<?php

use yii\db\Migration;

/**
 * Class m200414_023134_restaurant_menu_cat
 */
class m200414_023134_restaurant_menu_cat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('restaurant_menu_cat', [
            'id' => $this->primaryKey(),
            'restaurant_id' => $this->integer(11)->notNull(),
            'name_cat' => $this->string(62)->null(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('restaurant_menu_cat');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023134_restaurant_menu_cat cannot be reverted.\n";

        return false;
    }
    */
}
