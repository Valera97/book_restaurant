<?php

use yii\db\Migration;

/**
 * Class m200414_023201_order
 */
class m200414_023201_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'restaurant_id' => $this->integer(11)->notNull(),
            'restaurant_location_id' => $this->integer(11)->notNull(),
            'information' => $this->text()->notNull(),
            'date' => $this->integer(11)->notNull(),
            'status' => $this->integer(11)->notNull(),
            'people' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_023201_order cannot be reverted.\n";

        return false;
    }
    */
}
