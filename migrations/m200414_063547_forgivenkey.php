<?php

use yii\db\Migration;

/**
 * Class m200414_063547_forgivenkey
 */
class m200414_063547_forgivenkey extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'br-user-restaurant_id',
            'user',
            'restaurant_id',
            'restaurant',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-restaurant_menu_cat-restaurant_id',
            'restaurant_menu_cat',
            'restaurant_id',
            'restaurant',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-order-restaurant_id',
            'order',
            'restaurant_id',
            'restaurant',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-order-user_id',
            'order',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-order-restaurant_location_id',
            'order',
            'restaurant_location_id',
            'restaurant_location',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-restaurant_location-restaurant_id',
            'restaurant_location',
            'restaurant_id',
            'restaurant',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-restaurant_and_cat-restaurant_id',
            'restaurant_and_cat',
            'restaurant_id',
            'restaurant',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-restaurant_and_cat-cat_id',
            'restaurant_and_cat',
            'cat_id',
            'restaurant_cat',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-order_dishes-order_id',
            'order_dishes',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-order_dishes-dishes_id',
            'order_dishes',
            'dishes_id',
            'restaurant_menu_dishes',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-restaurant_menu_dishes-restaurant_id',
            'restaurant_menu_dishes',
            'restaurant_id',
            'restaurant',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'br-restaurant_menu_dishes-cat_id',
            'restaurant_menu_dishes',
            'cat_id',
            'restaurant_menu_cat',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'br-user-restaurant_id',
            'user'
        );

        $this->dropForeignKey(
            'br-restaurant_menu_cat-restaurant_id',
            'restaurant_menu_cat'
        );

        $this->dropForeignKey(
            'br-order-restaurant_id',
            'order'
        );

        $this->dropForeignKey(
            'br-order-user_id',
            'order'
        );

        $this->dropForeignKey(
            'br-order-restaurant_location_id',
            'order'
        );

        $this->dropForeignKey(
            'br-restaurant_location-restaurant_id',
            'restaurant_location'
        );

        $this->dropForeignKey(
            'br-restaurant_and_cat-cat_id',
            'restaurant_and_cat'
        );

        $this->dropForeignKey(
            'br-restaurant_and_cat-restaurant_id',
            'restaurant_and_cat'
        );

        $this->dropForeignKey(
            'br-order_dishes-order_id',
            'order_dishes'
        );

        $this->dropForeignKey(
            'br-order_dishes-dishes_id',
            'order_dishes'
        );

        $this->dropForeignKey(
            'br-restaurant_menu_dishes-restaurant_id',
            'restaurant_menu_dishes'
        );

        $this->dropForeignKey(
            'br-restaurant_menu_dishes-cat_id',
            'restaurant_menu_dishes'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_063547_forgivenkey cannot be reverted.\n";

        return false;
    }
    */
}
