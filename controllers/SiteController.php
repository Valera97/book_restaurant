<?php

namespace app\controllers;

use app\components\SiteHelper;
use app\models\Order;
use app\models\OrderForm;
use app\models\PayPalCheckout;
use app\models\Restaurant;
use app\models\RestaurantMenuDishes;
use app\models\User;
use app\modules\admin\models\Login;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['filter-restaurant'])) {
            $this->enableCsrfValidation = false;
        } elseif (in_array($action->id, ['menu'])) {
            $this->enableCsrfValidation = false;
        } elseif (in_array($action->id, ['get-order-price'])) {
            $this->enableCsrfValidation = false;
        } elseif (in_array($action->id, ['order-form'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }


        $model = new Login();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionSingUp()
    {
        $model = new \app\models\User();
        $model->access = User::ROLE_USER;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['login']);
        }

        return $this->render('sing-up', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionFilterRestaurant()
    {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('data_filter')) {
                $restaurant = SiteHelper::getRestaurantFilter(Yii::$app->request->post('data_filter'));

                return $restaurant;
            }
        }

        return 2;
    }

    public function actionMenu()
    {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('id_restaurant')) {
                $restaurant = SiteHelper::getMenuRestaurant(Yii::$app->request->post('id_restaurant'));

                return $restaurant;
            }
        }
    }

    public function actionRestaurant($id)
    {
        if (!empty($id)) {
            $model = Restaurant::find()->where(['id' => $id])->one();

            $model_filter = SiteHelper::getRestaurantAndCatById($id);
            $model_dishes = SiteHelper::getRestaurantDishes($id);

            return $this->render('restaurant', [
                'model' => $model,
                'model_filter' => $model_filter,
                'model_dishes' => $model_dishes,
            ]);
        }
    }

    public function actionOrder()
    {
        if (Yii::$app->request->isAjax) {
            $model = new Order();
            $model->load(Yii::$app->request->post());
            var_dump($model);
        }
    }

    public function actionGetOrderPrice()
    {
        if (Yii::$app->request->isAjax) {
            $price = 0;
            if (Yii::$app->request->post('data') != '') {
                $dishes_id = explode(",", Yii::$app->request->post('data'));
                $dishes_price = RestaurantMenuDishes::find()->select(['price'])->where(['in', 'id', $dishes_id])->all();

                if (!empty($dishes_price)) {
                    foreach ($dishes_price as $dishes) {
                        $price += (int)$dishes->price;
                    }
                }
            }

            return $price;
        }
    }

    public function actionOrderForm()
    {
       if (!empty(Yii::$app->request->post('restaurant_id'))) {
           $count_user = !empty(Yii::$app->request->post('count_user')) ? Yii::$app->request->post('count_user') : null;
           $restaurant_id = !empty(Yii::$app->request->post('restaurant_id')) ? Yii::$app->request->post('restaurant_id') : null;
           $information = !empty(Yii::$app->request->post('information')) ? Yii::$app->request->post('information') : null;
           $dishes_id = !empty(Yii::$app->request->post('dishes')) ? Yii::$app->request->post('dishes') : null;
           $price = 0;
           $dishes_array = [];

           if (!empty($dishes_id)) {
               $dishes = RestaurantMenuDishes::find()->where(['in', 'id', $dishes_id])->all();


               if (!empty($dishes)) {
                   foreach ($dishes as $dish) {
                       $price += (int)$dish->price;
                       array_push($dishes_array, $dish->id);
                   }
               }
           }

           $order_price = SiteHelper::discountPrice(Yii::$app->user->identity->id, $price);
           $model = new Order();
           $model->information = $information;
           $model->dishes = json_encode($dishes_array);
           $model->price = $order_price;
           $model->user_id = Yii::$app->user->identity->id;
           $model->restaurant_id = $restaurant_id;
           $model->people = $count_user;

           return $this->render('order-form', [
               'model' => $model,
               'count_user' => $count_user,
               'restaurant_id' => $restaurant_id,
               'dishes' => json_encode($dishes_array),
               'information' => $information,
               'restaurant_location_id' => 1,
               'order_price' => $order_price,
               'dishes_id' => $dishes_id,
               'price' => $price
           ]);
           exit();
       }
       echo "Error";
       exit();
    }

    /**
     *Save information for user
     */
    public function actionGetCookies()
    {

        if (Yii::$app->request->isAjax) {
            $arrayNameCookie = ['payer_email', 'payer_name', 'payer_last_name', 'payer_city', 'payer_city',
                'payer_street', 'payer_zip', 'sender_name', 'sender_last_name', 'sender_city', 'sender_street', 'sender_zip'];

            foreach ($arrayNameCookie as $name) {
                SiteHelper::addUserCookiesByPostName($name);
            }

            if (Yii::$app->request->post('order_id') && Yii::$app->request->post('order_amount')) {
                //check paypal order status
                $status = PayPalCheckout::getOrder(Yii::$app->request->post('order_id'), Yii::$app->request->post('order_amount'));
                if ($status['status'] === 'COMPLETED') {
                    SiteHelper::addUserCookiesByPostName('order_id');
                } else {
                    echo 'false';
                }
            }
        }
    }

    public function actionCreateOrder() {
        $model = new Order();
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['/admin/profile/order', 'id' => $model->id]);
        }
    }
}
