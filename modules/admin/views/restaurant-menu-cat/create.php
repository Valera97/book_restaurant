<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantMenuCat */

$this->title = Yii::t('app', 'Create Restaurant Menu Cat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Restaurant Menu Cats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-menu-cat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
