<table class="table">
    <tbody>
    <tr>
        <th scope="row">Ресторан</th>
        <td><?=$order->restaurant['name']?></td>
    </tr>
    <tr>
        <th scope="row">Время</th>
        <td><?=$order->date?></td>
    </tr>
    <tr>
        <th scope="row">Люди</th>
        <td><?=$order->people?></td>
    </tr>
    <tr>
        <th scope="row">Информация</th>
        <td><?=$order->information?></td>
    </tr>
    <?php if (!empty($order_dishes)) :?>
    <tr>
        <th scope="row">Цена</th>
        <td><?=$price?></td>
    </tr>
    <tr>
        <th scope="row">Блюда</th>
        <td>
            <?php foreach ($order_dishes as $dish) {
                $dishes = \app\models\RestaurantMenuDishes::find()->where(['id' => $dish->dishes_id])->one();
                echo $dishes->name.' - '.$dishes->price.'грн.<br>';
            }?>
        </td>
    </tr>
    <?php endif;?>
    </tbody>
</table>