<table class="table">
    <thead>
    <tr>
        <th scope="col">Ресторан</th>
        <th scope="col">Дата</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orders as $order) : ?>
    <tr>
        <th><?=$order->restaurant['name']?></th>
        <td><?=$order->date?></td>
        <td scope="row"><a href="<?=\yii\helpers\Url::to(['order', 'id'=>$order->id])?>">Подробнее</a></td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>