<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm; ?>

<?php $form = ActiveForm::begin(); ?>


        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'lastname')->textInput() ?>
        <?= $form->field($model, 'login')->textInput(['disabled' => true]) ?>
        <?= $form->field($model, 'password')->passwordInput(['value' => '']) ?>
        <?= $form->field($model, 'email')->textInput() ?>
        <?= $form->field($model, 'phone')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>