<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantMenuDishes */

$this->title = Yii::t('app', 'Create Restaurant Menu Dishes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Restaurant Menu Dishes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-menu-dishes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'category' => $category,
    ]) ?>

</div>
