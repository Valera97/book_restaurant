<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\User;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->user->isGuest) {
        if (Yii::$app->user->identity->access == User::ROLE_ROOT) {
            $items = [
                ['label' => 'Home', 'url' => ['/admin/profile/index']],
                ['label' => 'Restaurant Category', 'url' => ['/admin/restaurant-cat/index']],
                ['label' => 'All Restaurant', 'url' => ['/admin/restaurant/index']],
                ['label' => 'All Order', 'url' => ['/admin/order/index']],
                ['label' => 'Logout (' . Yii::$app->user->identity->login . ')', 'url' => ['/admin/default/exit']]
            ];
        } elseif (Yii::$app->user->identity->access == User::ROLE_ADMIN) {
            $items = [
                ['label' => 'Home', 'url' => ['/admin/profile/index']],
                ['label' => 'Restaurant Info', 'url' => ['/admin/restaurant/index']],
                ['label' => 'Restaurant Location', 'url' => ['/admin/restaurant-location/index']],
                ['label' => 'Restaurant Menu Categories', 'url' => ['/admin/restaurant-menu-cat/index']],
                ['label' => 'Restaurant Dishes', 'url' => ['/admin/restaurant-menu-dishes/index']],
                ['label' => 'Restaurant Cat', 'url' => ['/admin/restaurant-and-cat/index']],
                ['label' => 'Order', 'url' => ['/admin/order/index']],
                ['label' => 'Logout (' . Yii::$app->user->identity->login . ')', 'url' => ['/admin/default/exit']]
            ];
        } else {
            $items = [
                ['label' => 'Home', 'url' => ['/admin/profile/index']],
                ['label' => 'Orders', 'url' => ['/admin/profile/orders']],
                ['label' => 'Logout (' . Yii::$app->user->identity->login . ')', 'url' => ['/admin/default/exit']]
            ];
        }
    } else {
        $items = [
            ['label' => 'Login', 'url' => ['/admin/default/index']]
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
