<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Restaurant',
                'value' => function (\app\models\Order $model) {
                    return $model->restaurant['name'];
                },
            ],
            [
                'attribute' => 'User',
                'value' => function (\app\models\Order $model) {
                    return $model->user['name'];
                },
            ],
            'information:ntext',
            //'date',
            //'status',
            //'people',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                    },
                    'delete' => function ($url, $model, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', $url);
                    },
                ],
            ]
        ]
    ]);?>


</div>
