<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantAndCat */
/* @var $form yii\widgets\ActiveForm */
$items = \yii\helpers\ArrayHelper::map($category,'id','name');
$params = [
    'prompt' => 'Choose Cat'
];
?>

<div class="restaurant-and-cat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cat_id')->dropDownList($items,$params) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
