<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Restaurant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="restaurant-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'small_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'main_photo')->widget(
        FileInput::classname(),[
        'options'=>[
            'multiple'=>false
        ],
        'pluginOptions' => [
            'showUpload' => false,
            'maxFileSize'=>30000
        ]
    ]); ?>

    <?= $form->field($model, 'gallery')->widget(
        FileInput::classname(),[
        'options'=>[
            'multiple'=>true
        ],
        'pluginOptions' => [
            'showUpload' => false,
            'maxFileSize'=>1000000
        ]
    ]);?>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
