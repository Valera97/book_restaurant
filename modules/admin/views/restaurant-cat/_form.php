<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantCat */
/* @var $form yii\widgets\ActiveForm */
$items = \yii\helpers\ArrayHelper::map($category,'id','name');
$params = [
    'prompt' => [
            'text' => 'No',
            'options' => [
                'value' => \app\models\RestaurantCat::PARENT_CAT,
                ]
    ],
];
?>

<div class="restaurant-cat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->dropDownList($items,$params) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
