<?php


namespace app\modules\admin\controllers;


use app\models\Order;
use app\models\OrderDishes;
use app\models\Paypal;
use app\models\User;
use Yii;
use yii\web\Controller;

class ProfileController extends Controller
{
    public function actionIndex() {
        $model = User::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $this->layout = 'main';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->render('index', [
           'model' => $model,
        ]);
    }

    public function actionOrder($id) {
        $order = Order::find()->where(['id' => $id])->one();
        $price = 0;
        $order_dishes = OrderDishes::find()->where(['order_id' => $id])->all();
        if (!empty($order_dishes)) {
            $paypal = Paypal::find()->where(['order_id' => $id])->one();
            if (!empty($paypal)) {
                $price = $paypal->order_amount;
            }
        }

        return $this->render('order',[
            'order' => $order,
            'order_dishes' => $order_dishes,
            'price' => $price
        ]);
    }

    public function actionOrders(){
        $orders = Order::find()->where(['user_id' => Yii::$app->user->id])->all();

        return $this->render('orders', [
           'orders' => $orders
        ]);
    }
}