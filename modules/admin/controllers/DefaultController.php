<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Login;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'main';
        $model = new Login();
        if (!Yii::$app->user->isGuest || ($model->load(Yii::$app->request->post()) && $model->login())) {
            return $this->redirect(['profile/index']);
        }


        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionExit()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/admin/default/']);
    }
}
