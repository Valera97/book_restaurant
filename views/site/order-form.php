<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$formattedDate = !empty($model->date) ? Yii::$app->formatter->asDatetime($model->public_date, 'php:d.m.Y H:i') : '';
var_dump($dishes);
?>
<div class="container">
<?php $form = ActiveForm::begin(['action' => 'create-order']); ?>

<?= $form->field($model, 'user_id')->hiddenInput()->label(false);?>

<?= $form->field($model, 'restaurant_id')->hiddenInput()->label(false); ?>

<?= $form->field($model, 'information')->textarea(['rows' => 6,'disabled' => true]) ?>

<?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
    'options' => [
        'value' => $formattedDate,
        'placeholder' => 'Выберете дату и время',
    ],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd.mm.yyyy HH:ii',
        'todayHighlight' => true,
        'todayBtn' => true,
    ]
]); ?>


<?= $form->field($model, 'people')->textInput(['disabled' => true]) ?>
<?= $form->field($model, 'dishes')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'people')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'price')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'restaurant_location_id')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'information')->hiddenInput()->label(false); ?>

<?php if($price != 0) :?>
    <div class="checkout__step-wrap paypal" >
        <div id="paypal-button-container"></div>
    </div>
<?php endif;?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>


</div>
<?php
$url_cookies = Url::to('get-cookies');
$price_paypal = $order_price;
$url_form = Url::to(['create_order']);


$js = <<< JS
 
//Paypal
var isPaid = false;
paypal.Buttons({
        createOrder: function(data, actions) {
            // Set up the transaction
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: $price_paypal,
                        currency: 'EUR'
                    }
                }]
            });
        },
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                console.log(details);
                isPaid = true;
                $('.checkout__step-2').removeClass('not-filled');
                var nextStep = $('.checkout__step-3 .checkout__step-wrap');
                nextStep.slideDown();
                nextStep.addClass('slided');
                var order_id = data.orderID;

                $.ajax({
                    url: '$url_cookies',
                    method: 'post',
                    data:({
                            order_amount : $price_paypal,
                            order_id: order_id,
                    }),
                    success: function(data){
                        if (data == 'false'){
                            alert('payment failed');
                        }
                    }
                })
            });
        }
    }).render('#paypal-button-container');



JS;

$this->registerJs($js, yii\web\View::POS_END);