<?php

use kartik\select2\Select2;
use yii\widgets\ActiveForm;

if (isset($_GET['type'])) {
    echo '<script> $(document).ready(function(){var modal = document.getElementById("myModal"); modal.style.display = "block";})</script>';
}
?>
    <div class="clear_first" class="img-fluid">
        <div id="paral">

        </div>
    </div>
    <div class="clear_second">
    <div class="container" style="position: center; background-color: #c85044">
        <div>
            <div class="row" style="padding: 3px; position: relative;">
                <div class="col-9" style="background-color: #F7C1D7">


                    <h2 class="container" style="margin:7px; padding-top: 5px"><?= $model->name ?></h2>

                    <div class="container">
                        <div>
                            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                                </ol>
                                <div class="container">
                                    <div class="carousel-inner lazy" style="">
                                        <?php if (!empty($model->gallerys)) : ?>
                                        <?php $index = 0; ?>
                                        <?php foreach ($model->gallerys as $mode) : ?>
                                        <?php if ($index == 0) : ?>
                                        <?php $index++ ?>
                                        <div class="carousel-item active">
                                            <?php else: ?>
                                            <div class="carousel-item">
                                                <?php endif; ?>
                                                <img src="<?= \yii\helpers\Url::base(true) . $mode->file_path ?>"
                                                     class="d-block w-100" alt="...">
                                            </div>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                            </div>
                            <p style="margin-top:10px;"><?= $model->description ?></p>
                        </div>

                    </div>


                    <div class="col-3" style="background-color: #F9D4E3">
                        <div>
                            <div style="margin-top: 20px; margin-bottom:10px ">
                                <button type="button" onclick="add_order(<?= $model['id'] ?>)"
                                        class="btn btn-danger btn-lg btn-block" style="position: center">ЗАБРОНИРОВАТЬ
                                </button>

                            </div>

                            <div class="filters">
                                <div class="filter1">
                                    <div style="margin-top: 20px;">ВРЕМЯ РАБОТЫ: 8:00-22:00</div>
                                    <?php if (!empty($model_filter['parent'])) : ?>
                                        <div style="margin-top: 10px">
                                            <?php foreach ($model_filter['parent'] as $parent) : ?>
                                                <div style="padding-bottom: 20px">
                                                    <h5><?= $parent['name'] ?></h5>
                                                    <p>
                                                        <?php foreach ($model_filter['cat_restaurant'] as $child) {
                                                            if ($child['parent_id'] == $parent['id']) {
                                                                echo $child['name'] . ',';
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                    <div style="margin-top: 10px">
                                    </div>
                                    <div style="margin-top: 10px">
                                        <div style="padding-bottom: 20px">
                                            <img src="../../img/1.jpg" width="250"
                                                 height="150" border="0" alt="Пример2">
                                            картинка для тебя
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <div class="geocard" style="margin-bottom:20px ; margin-top: 15px;">
                                <div style="padding-bottom: 10px;">
                                    <img src="../../img/картафейк.png" width="250"
                                         height="150" border="0" alt="Пример1">

                                </div>
                                <button type="button" class="btn btn-warning" style="height: 40px;
                   display:block;
                   position: relative;
                   border: 1px solid white;
                   margin:0 auto;">Местоположение
                                </button>

                            </div>


                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close" onclick="display_modal()">&times;</span>
            <?php if (Yii::$app->user->isGuest) : ?>
                You Guest
            <?php else : ?>

                <form method="post" action="<?=Yii::$app->request->baseUrl?>/site/order-form">
                    <div class="form">
                        <div class="col-md-4 mb-4">
                            <label for="validationServer01">Имя</label>
                            <input type="text" name="name_user" class="form-control is-valid" id="validationServer01" required>
                            <input type="hidden" name="restaurant_id" value="<?=$model->id?>" required>
                        </div>
                        <div class="col-md-4 mb-4">
                            <label for="validationServer02">Кол-во человек</label>
                            <input type="text" name="count_user" class="form-control is-valid"
                                   id="validationServer02" required>
                        </div>
                        <div class="form-group col-md-4 mb-4">
                            <label for="localdate">Дата и время: </label>
                            <input type="datetime-local" name="date" id="localdate" name="date"/>

                        </div>
                        <div class="col-md-4 mb-4">
                            <label for="validationTextarea">Textarea</label>
                            <textarea class="form-control is-invalid" name="information" id="validationTextarea" placeholder="Required example textarea" required></textarea>
                        </div>
                        <?php if (!empty($model_dishes)) : ?>
                            <div class="custom-control custom-checkbox col-md-4 mb-4">
                                <input type="checkbox" class="custom-control-input" id="customControlValidation1">
                                <label class="custom-control-label" for="customControlValidation1">Заказать еду</label>
                            </div>
                            <div class="form-group col-md-4 mb-4" id="select_dishes" style="display: none">
                                <select class="custom-select js-example-basic-multiple" id="menu-selector" name="dishes[]" multiple="multiple">
                                    <?php if (!empty($model_dishes)) :?>
                                    <?php foreach ($model_dishes as $dishes) :?>
                                   <option value="<?=$dishes->id?>" data-price="<?=$dishes->price?>"><?=$dishes->name?> - <?=$dishes->price?> грн.</option>
                                    <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                            </div>
                            <br>
                            <br>
                            <div class="input-group mb-3" id="order_price" style="display: none">
                                <input type="text" class="form-control" id="order_price_value" disabled value="0">
                            </div>
                        <?php endif; ?>
                    </div>



                    <button class="btn btn-primary" type="submit">Submit form</button>
                </form>
            <?php endif; ?>
        </div>

    </div>
    <style>

        .carousel {
            height: 300px !important;
            overflow: hidden;
        }

        .carousel-inner {
            overflow: visible;
        }

        .carousel-inner img {
            left: 50%;
            max-width: none !important;
            min-height: 300px;
            min-width: 50%;
            position: absolute;
            top: 200px;
            transform: translate(-50%, -50%);
        }

        .clear_first {
            position: relative;
            background: url(../../img/1cafe2.jpg) 600px 100%;
            opacity: 0.4;
            height: 500px;
            width: 100%;
            top: 0;
        }

        .clear_second {
            position: relative;
            background: #F7C1D7;
            opacity: 1;
            height: 1000px;
            width: 100%;
            top: 0;
        }

        .clear_third {
            position: relative;
            background: #ccc;
            opacity: 0.9;
            height: 700px;
            width: 100%;
            top: 0;
        }

        #paral {
            position: absolute;
            background: ;
            background-size: 100% 200px;
            height: 100px;
            width: 100%;
            margin: 0 auto;
            left: 0;
            bottom: 0;
            border-top: 1px solid;
        }
    </style>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>


<?php
$url_filter = Yii::$app->request->baseUrl . '/site/filter-restaurant';
$url_menu = Yii::$app->request->baseUrl . '/site/menu';
$url_order = Yii::$app->request->baseUrl . '/site/order';
$url_price = Yii::$app->request->baseUrl . '/site/get-order-price';
$js = <<< JS
$(document).ready(function() {
    jQuery('.js-example-basic-multiple').select2();
});
const checkbox = document.getElementById('customControlValidation1')

$('#menu-selector').on('change', function (e) {
    var data = $('#menu-selector').val();
    var formData = new FormData();
    formData.append('data', data);
    jQuery.ajax({
                url: '$url_price',
                type: 'POST',
                data: formData,
                success: function (data) {
                    document.getElementById('order_price_value').value = data;
                },
                error: function (data) {
                    console.log(data);
                }, 
                cache: false,
                contentType: false,
                processData: false
            });
    console.log($('#menu-selector').val());
});

if (checkbox != null) {
checkbox.addEventListener('change', (event) => {
  if (event.target.checked) {
    $('#select_dishes').show();
    $('#order_price').show();
    $('#menu-selector').select2();
  } else {
    $('#select_dishes').hide();
    $('#order_price').hide();
  }
})
}
 $('.order_form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            jQuery.ajax({
                url: '$url_order',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }, 
                cache: false,
                contentType: false,
                processData: false
            });
        });

 function add_order(data) {
    var formData = new FormData();
          formData.append('id_restaurant', data);
          $.ajax({
                    url: '$url_menu',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(data){
                        $('#menu-selector').html(data.responseText);
                    },
                    error: function(data) {
                        console.log(data);
                        document.getElementById('menu-selector').innerHTML = data.responseText;
                    }
    })
    console.log(data)
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
 }
 function display_modal() {
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
 }
 window.onclick = function(event) {
    var modal = document.getElementById("myModal");
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
JS;

$this->registerJs($js, yii\web\View::POS_END);