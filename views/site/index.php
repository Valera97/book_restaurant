<?php
use app\components\SiteHelper;
use yii\helpers\Url;

$parent_filter = SiteHelper::getAllParentFilterRestaurant();
?>
<div class="col-12 col-md-10 "  style="display: inline-block; ; text-align: center; margin-left: 7%; margin-right: 7%; margin-top: 10px; background-color: #F7C1D7" >
        <h3 style="margin-bottom: 60px">RESTORANI ODESSA</h3>

        <div class="container ">
            <div class="row">
                <div class="col-9" id="restaurant" style="background-color: #F7C1D7">
                    <div>
                        <div>
                            <div style="display: flex;  flex-flow: row wrap;">
                                <p>POISK PO TIPU KUCHNI</p>
                                <a href="список ресторанов" style="margin-left: auto;">еще</a>
                            </div>
                            <div class="container">

                                <div class="row">
                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/meat.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>

                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/vegan.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>


                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/pizza.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>

                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/meat.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/vegan.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>

                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/pizza.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>


                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/vegan.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>

                                    <div class="" style="margin-left: 5px; margin-top: 10px; margin-right: 5px;">
                                        <a href="">
                                            <img src="img/meat.jpg" style="height: 120px; width: 190px" alt="">

                                        </a>
                                    </div>

                                </div>


                                <style>

                                    .jam {
                                        width: 100%;
                                        display: flex;
                                        display: -webkit-flex;
                                        justify-content: space-around;
                                        -webkit-justify-content: space-around;
                                    }

                                    .description {
                                        margin-top: 5px;
                                        display: flex;
                                        display: -webkit-flex;
                                        -webkit-justify-content: center;
                                        justify-content: center;
                                        -webkit-flex-direction: column;
                                        flex-direction: column;
                                        height: 85px;
                                        text-align: center;
                                    }

                                    .description1 {
                                        display: flex;
                                        display: -webkit-flex;
                                        -webkit-justify-content: center;
                                        justify-content: center;
                                        -webkit-flex-direction: column;
                                        flex-direction: column;
                                        height: 50px;

                                        text-align: center;
                                    }



                                </style>

                            </div>

                        </div>



                        <div style="background-color: #FBD0E1;">
                            <div style="display: flex;  flex-flow: row wrap; margin-top: 20px;">
                                <p>ВКУСНО И НЕДОРОГО</p>
                                <a href="список ресторанов" style="margin-left: auto;">еще</a>
                            </div>
                            <div class="container">

                                <div class="row">
                                    <div class="card" style="margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                        <a href="">
                                            <img src="img/1.jpg" style="height: 110px; width: 170px" alt="">
                                            <div class="description">
                                                <p>
                                                    название<br>
                                                    цена<br>
                                                    <button type="button" class="btn btn-danger btn-sm btn-block" style="position: center">ЗАБРОНИРОВАТЬ</button>
                                                </p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="card" style="margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                        <a href="">
                                            <img src="img/vegan.jpg" style="height: 110px; width: 170px" alt="">
                                            <div class="description">
                                                <p>
                                                    Надпись<br>
                                                    еще строка<br>
                                                    <button type="button" class="btn btn-danger btn-sm btn-block" style="position: center">ЗАБРОНИРОВАТЬ</button>
                                                </p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="card" style="margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                        <a href="">
                                            <img src="img/pizza.jpg" style="height: 110px; width: 170px" alt="">
                                            <div class="description">
                                                <p>
                                                    название<br>
                                                    цена<br>
                                                    <button type="button" class="btn btn-danger btn-sm btn-block" style="position: center">ЗАБРОНИРОВАТЬ</button>
                                                </p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="card" style="margin-left: 10px; margin-top: 10px; margin-right: 10px;">
                                        <a href="">
                                            <img src="img/vegan.jpg" style="height: 110px; width: 170px" alt="">
                                            <div class="description">
                                                <p>
                                                    Надпись<br>
                                                    еще строка<br>
                                                    <button type="button" class="btn btn-danger btn-sm btn-block" style="position: center">ЗАБРОНИРОВАТЬ</button>
                                                </p>
                                            </div>
                                        </a>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-9 row" id="restaurant_filter" style="background-color: #F7C1D7; display: none;">
                    test
                </div>

                <div class="col-3" style="background-color: #F9D4E3">
                    <div style="" >
                        <div class="geocard" style="margin-right: 10px; margin-top: 15px;">
                            <div style="padding-bottom: 20px;">
                                <img src="img/картафейк.png" width="250"
                                     height="150" border="0" alt="Пример1">

                            </div>
                            <div>карта с ресторанами</div>

                        </div>

                        <div class="filters">
                            <?php if (!empty($parent_filter)):?>

                                <?php foreach ($parent_filter as $filter) :?>
                                    <?php $child_filter = SiteHelper::getAllChildFilterRestaurant($filter->id)?>

                                    <?php if (!empty($child_filter)) :?>
                                        <div class="filter1">
                                            <div style="margin-top: 10px">
                                                <div style="display: flex;"><?=$filter->name?>:</div>
                                                <?php foreach ($child_filter as $child) :?>
                                                    <div class="form-check" style="display: flex;  align-items: center;">
                                                        <input class="form-check-input filter_box" type="checkbox" onclick="filter()" id="chek_box_<?=$child->id?>" name="filter_box"  value="<?=$child->id?>">
                                                        <label class="form-check-label" for="chek_box_<?=$child->id?>"><?=$child->name?></label>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                            <div style="margin-top: 10px">
                                <div style="padding-bottom: 20px">
                                    <img src="img/1.jpg" width="250"
                                         height="150" border="0" alt="Пример2">

                                </div>

                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
</div>
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close" onclick="display_modal()">&times;</span>
            <?php if(Yii::$app->user->isGuest) :?>
            You Guest
            <?php else :?>
                <form>
                    <div class="form">
                        <div class="col-md-4 mb-4">
                            <label for="validationServer01">Имя</label>
                            <input type="text" name = "Order[name_user]" class="form-control is-valid" id="validationServer01" required>
                        </div>
                        <div class="col-md-4 mb-4">
                            <label for="validationServer02">Кол-во человек</label>
                            <input type="text" name = "Order[count_user]" class="form-control is-valid" id="validationServer02" required>
                        </div>
                        <div class="form-group col-md-4 mb-4">
                            <label for="localdate">Дата и время: </label>
                            <input type="datetime-local" name = "Order[date]" id="localdate" name="date"/>

                        </div>
                        <div class="col-md-4 mb-4">
                            <label for="validationTextarea">Textarea</label>
                            <textarea class="form-control is-invalid" name = "Order[information]" id="validationTextarea" placeholder="Required example textarea" required></textarea>
                        </div>

                        <div class="custom-control custom-checkbox col-md-4 mb-4">
                            <input type="checkbox" class="custom-control-input" id="customControlValidation1" required>
                            <label class="custom-control-label" for="customControlValidation1">Заказать еду</label>
                        </div>

                        <div class="form-group col-md-4 mb-4" id="select_dishes" style="display: none">
                            <select class="custom-select" id="menu-selector" name = "Order[dishes]" required>
                                <option selected>выберите блюдо, которое хотите заказать</option>
                            </select>
                        </div>
                    </div>


                    <button class="btn btn-primary" type="submit">Submit form</button>
                </form>
            <?php endif;?>
        </div>

    </div>
<?php
$url_filter = Yii::$app->request->baseUrl. '/site/filter-restaurant';
$url_menu = Yii::$app->request->baseUrl. '/site/menu';
$js = <<< JS

const checkbox = document.getElementById('customControlValidation1')

checkbox.addEventListener('change', (event) => {
  if (event.target.checked) {
    $('#select_dishes').show();
  } else {
    $('#select_dishes').hide();
  }
})

 function filter() {
    var inputElements = document.getElementsByClassName('filter_box');
      var array_product_id_delete = [];
      if (inputElements.length > 0) {
          for (var i = 0; i < inputElements.length; ++i) {
              if (inputElements[i].checked) {
                  checkedValue = inputElements[i].value;
                  array_product_id_delete.push(checkedValue);
              }
          }
      }
      
      if (array_product_id_delete.length > 0) {
          var formData = new FormData();
          formData.append('data_filter', array_product_id_delete);
          $('#restaurant_filter').show();
          $('#restaurant').hide();
          $.ajax({
                    url: '$url_filter',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(data){
                        $('#restaurant_filter').html(data);
                        console.log(1);
                    },
                    error: function(data) {
                        console.log(data);
                        document.getElementById('restaurant_filter').innerHTML = data.responseText;
                    }
    })
      } else {
          $('#restaurant_filter').hide();
          $('#restaurant').show();
      }
 }
 
 function add_order(data) {
    var formData = new FormData();
          formData.append('id_restaurant', data);
          $.ajax({
                    url: '$url_menu',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(data){
                        $('#menu-selector').html(data.responseText);
                    },
                    error: function(data) {
                        console.log(data);
                        document.getElementById('menu-selector').innerHTML = data.responseText;
                    }
    })
    console.log(data)
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
 }
 function display_modal() {
    modal.style.display = "none";
 }
 window.onclick = function(event) {
    var modal = document.getElementById("myModal");
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
JS;

$this->registerJs($js, yii\web\View::POS_END);
