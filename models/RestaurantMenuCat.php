<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "restaurant_menu_cat".
 *
 * @property int $id
 * @property int $restaurant_id
 * @property string|null $name_cat
 *
 * @property Restaurant $restaurant
 * @property RestaurantMenuDishes[] $restaurantMenuDishes
 */
class RestaurantMenuCat extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETE = 3;


    public static function getArrayStatusLabel()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
            self::STATUS_DELETE => Yii::t('app', 'Delete'),
        ];
    }

    public static function getArrayNotDeletedStatusLabel()
    {
        $statuses = RestaurantMenuCat::getArrayStatusLabel();
        unset($statuses[self::STATUS_DELETE]);

        return $statuses;
    }

    public static function getStatusLabel($status)
    {
        $status = (int)$status;
        $arrayStatusesLabel = self::getArrayStatusLabel();
        return !empty($arrayStatusesLabel[$status]) ? $arrayStatusesLabel[$status] : $arrayStatusesLabel[self::STATUS_ACTIVE];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_menu_cat';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'value' => Yii::$app->user->identity->restaurant_id,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['restaurant_id'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['restaurant_id'], 'integer'],
            [['name_cat'], 'string', 'max' => 62],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'restaurant_id' => Yii::t('app', 'Restaurant ID'),
            'name_cat' => Yii::t('app', 'Name Cat'),
        ];
    }

    /**
     * Gets query for [[Restaurant]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurant_id']);
    }

    /**
     * Gets query for [[RestaurantMenuDishes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantMenuDishes()
    {
        return $this->hasMany(RestaurantMenuDishes::className(), ['cat_id' => 'id']);
    }
}
