<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "restaurant_cat".
 *
 * @property int $id
 * @property string $name
 * @property int $parent_id
 *
 * @property RestaurantAndCat[] $restaurantAndCats
 */
class RestaurantCat extends \yii\db\ActiveRecord
{
    const PARENT_CAT = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_cat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id'], 'safe'],
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 62],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'parent_id' => Yii::t('app', 'Parent ID'),
        ];
    }

    /**
     * Gets query for [[RestaurantAndCats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantAndCats()
    {
        return $this->hasMany(RestaurantAndCat::className(), ['cat_id' => 'id']);
    }
}
