<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_dishes".
 *
 * @property int $id
 * @property int $order_id
 * @property int $dishes_id
 *
 * @property RestaurantMenuDishes $dishes
 * @property Order $order
 */
class OrderDishes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_dishes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'dishes_id'], 'required'],
            [['order_id', 'dishes_id'], 'integer'],
            [['dishes_id'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantMenuDishes::className(), 'targetAttribute' => ['dishes_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'dishes_id' => Yii::t('app', 'Dishes ID'),
        ];
    }

    /**
     * Gets query for [[Dishes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDishes()
    {
        return $this->hasOne(RestaurantMenuDishes::className(), ['id' => 'dishes_id']);
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
