<?php

namespace app\models;


use Yii;
use yii\base\Model;

class OrderForm extends Model
{

    public $count_people;
    public $date;
    public $menu_dishes;
    public $information;

    public function rules()
    {
        return [
            [['count_people', 'date'], 'required'],
            [['information', 'menu_dishes'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'count_people' => Yii::t('app', 'Count People'),
            'menu_dishes' => Yii::t('app', 'Menu Dishes'),
            'information' => Yii::t('app', 'Information'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
