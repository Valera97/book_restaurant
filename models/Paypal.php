<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paypal".
 *
 * @property int $id
 * @property string $paypal_order_id
 * @property string $paypal_status
 * @property float $paypal_amount
 * @property float $order_amount
 * @property int|null $order_id
 * @property string $hash
 * @property string $paypal_payer_id
 * @property int $created_at
 * @property int $updated_at
 */
class Paypal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paypal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paypal_order_id', 'paypal_status', 'paypal_amount', 'order_amount', 'hash', 'paypal_payer_id', 'created_at', 'updated_at'], 'required'],
            [['paypal_amount', 'order_amount'], 'number'],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['paypal_order_id', 'paypal_status', 'paypal_payer_id'], 'string', 'max' => 127],
            [['hash'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'paypal_order_id' => Yii::t('app', 'Paypal Order ID'),
            'paypal_status' => Yii::t('app', 'Paypal Status'),
            'paypal_amount' => Yii::t('app', 'Paypal Amount'),
            'order_amount' => Yii::t('app', 'Order Amount'),
            'order_id' => Yii::t('app', 'Order ID'),
            'hash' => Yii::t('app', 'Hash'),
            'paypal_payer_id' => Yii::t('app', 'Paypal Payer ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
