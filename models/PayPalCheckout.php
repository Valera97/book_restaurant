<?php
/**
 * Created by PhpStorm.
 * User: laptop
 * Date: 20.08.2019
 * Time: 18:33
 */

namespace app\models;


use app\components\SiteHelper;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use Yii;

class PayPalCheckout
{
    public static function getOrder($orderId, $order_amount)
    {
        $paypal_order = Paypal::findOne(['paypal_order_id' => $orderId]);

        if (empty($paypal_order)) {
            $client = PayPalClient::client();
            //Creating a pdf document and calculating its pages
            $response = $client->execute(new OrdersGetRequest($orderId));

            //Putting a response from paypal into a paypal table
            $new_paypal = new Paypal();
            $new_paypal->paypal_order_id = $response->result->id;
            $new_paypal->paypal_payer_id = $response->result->payer->payer_id;
            $new_paypal->paypal_status = $response->result->status;
            $new_paypal->paypal_amount = $response->result->purchase_units[0]->amount->value;
            $new_paypal->order_amount = $order_amount;
            $new_paypal->hash = md5($response->result->id . '-' . date('Y-m-d H-i-s'));

            if (!empty($response->result->purchase_units[0])) {
                if (!empty($response->result->purchase_units[0]->payments->captures[0])) {
                    $new_paypal->created_at = strtotime($response->result->purchase_units[0]->payments->captures[0]->create_time);
                    $new_paypal->updated_at = strtotime($response->result->purchase_units[0]->payments->captures[0]->update_time);
                }
            }
            $new_paypal->save();
            SiteHelper::addUserCookies('hash', $new_paypal->hash);

            return [
                'status' => $response->result->status,
                'paypal_amount' => $response->result->purchase_units[0]->amount->value
            ];
        }

        return [];
    }
}

/**
 *This driver function invokes the getOrder function to retrieve
 *sample order details.
 *
 *To get the correct order ID, this sample uses createOrder to create a new order
 *and then uses the newly-created order ID with GetOrder.
 */
if (!count(debug_backtrace())) {
    GetOrder::getOrder('REPLACE-WITH-ORDER-ID', true);
}
