<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "restaurant_menu_dishes".
 *
 * @property int $id
 * @property int $restaurant_id
 * @property int $cat_id
 * @property string $name
 * @property string $description
 * @property float $price
 * @property int $status
 *
 * @property OrderDishes[] $orderDishes
 * @property RestaurantMenuCat $cat
 * @property Restaurant $restaurant
 */
class RestaurantMenuDishes extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETE = 3;


    public static function getArrayStatusLabel()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
            self::STATUS_DELETE => Yii::t('app', 'Delete'),
        ];
    }

    public static function getArrayNotDeletedStatusLabel()
    {
        $statuses = RestaurantMenuDishes::getArrayStatusLabel();
        unset($statuses[self::STATUS_DELETE]);

        return $statuses;
    }

    public static function getStatusLabel($status)
    {
        $status = (int)$status;
        $arrayStatusesLabel = self::getArrayStatusLabel();
        return !empty($arrayStatusesLabel[$status]) ? $arrayStatusesLabel[$status] : $arrayStatusesLabel[self::STATUS_ACTIVE];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_menu_dishes';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'value' => Yii::$app->user->identity->restaurant_id,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['restaurant_id'],
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'value' => self::STATUS_ACTIVE,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['status'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'name', 'description', 'price'], 'required'],
            [['restaurant_id', 'cat_id', 'status'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 62],
            [['description'], 'string', 'max' => 255],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantMenuCat::className(), 'targetAttribute' => ['cat_id' => 'id']],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'restaurant_id' => Yii::t('app', 'Restaurant ID'),
            'cat_id' => Yii::t('app', 'Cat ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[OrderDishes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDishes()
    {
        return $this->hasMany(OrderDishes::className(), ['dishes_id' => 'id']);
    }

    /**
     * Gets query for [[Cat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(RestaurantMenuCat::className(), ['id' => 'cat_id']);
    }

    /**
     * Gets query for [[Restaurant]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurant_id']);
    }
}
