<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "restaurant_location".
 *
 * @property int $id
 * @property int $restaurant_id
 * @property string $address
 * @property string $phone
 * @property int $status
 *
 * @property Order[] $orders
 * @property Restaurant $restaurant
 */
class RestaurantLocation extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETE = 3;

    public static function getArrayStatusLabel()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
            self::STATUS_DELETE => Yii::t('app', 'Delete'),
        ];
    }

    public static function getArrayNotDeletedStatusLabel()
    {
        $statuses = Restaurant::getArrayStatusLabel();
        unset($statuses[self::STATUS_DELETE]);

        return $statuses;
    }

    public static function getStatusLabel($status)
    {
        $status = (int)$status;
        $arrayStatusesLabel = self::getArrayStatusLabel();
        return !empty($arrayStatusesLabel[$status]) ? $arrayStatusesLabel[$status] : $arrayStatusesLabel[self::STATUS_ACTIVE];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_location';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'value' => self::STATUS_ACTIVE,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['status'],
                ],
            ],
            [
                'class' => AttributeBehavior::className(),
                'value' => Yii::$app->user->identity->restaurant_id,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['restaurant_id'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'phone'], 'required'],
            [['restaurant_id', 'status'], 'integer'],
            [['address', 'phone'], 'string'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'restaurant_id' => Yii::t('app', 'Restaurant ID'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['restaurant_location_id' => 'id']);
    }

    /**
     * Gets query for [[Restaurant]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurant_id']);
    }
}
