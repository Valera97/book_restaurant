<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "restaurant_gallery".
 *
 * @property int $id
 * @property int $restaurant_id
 * @property string $file_path
 */
class RestaurantGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'file_path'], 'required'],
            [['restaurant_id'], 'integer'],
            [['file_path'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'restaurant_id' => Yii::t('app', 'Restaurant ID'),
            'file_path' => Yii::t('app', 'File Path'),
        ];
    }
}
