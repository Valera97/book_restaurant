<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "restaurant_and_cat".
 *
 * @property int $id
 * @property int $restaurant_id
 * @property int $cat_id
 *
 * @property RestaurantCat $cat
 * @property Restaurant $restaurant
 */
class RestaurantAndCat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_and_cat';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'value' => Yii::$app->user->identity->restaurant_id,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['restaurant_id'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id'], 'required'],
            [['restaurant_id', 'cat_id'], 'integer'],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantCat::className(), 'targetAttribute' => ['cat_id' => 'id']],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'restaurant_id' => Yii::t('app', 'Restaurant ID'),
            'cat_id' => Yii::t('app', 'Cat ID'),
        ];
    }

    /**
     * Gets query for [[Cat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(RestaurantCat::className(), ['id' => 'cat_id']);
    }

    /**
     * Gets query for [[Restaurant]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurant_id']);
    }
}
