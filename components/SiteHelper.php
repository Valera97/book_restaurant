<?php
namespace app\components;

use app\models\Paypal;
use app\models\Restaurant;
use app\models\RestaurantAndCat;
use app\models\RestaurantCat;
use app\models\RestaurantGallery;
use app\models\RestaurantMenuCat;
use app\models\RestaurantMenuDishes;
use app\models\User;
use DateTime;
use Yii;
use yii\helpers\Url;

class SiteHelper
{
    public static function getAllParentFilterRestaurant() {
        $model_filter = \app\models\RestaurantCat::find()->where(['parent_id' => 0])->all();

        if (!empty($model_filter)) {
            return $model_filter;
        }

        return false;
    }

    public static function getAllChildFilterRestaurant($parrent_id) {
        $model_filter_child = \app\models\RestaurantCat::find()->where(['parent_id' => $parrent_id])->all();

        if (!empty($model_filter_child)) {
            return $model_filter_child;
        }
         return false;
    }

    public static function getRestaurantFilter($filter_array) {
        $model_result = Yii::$app->db->createCommand('SELECT restaurant.* FROM restaurant_and_cat LEFT JOIN restaurant ON restaurant.id = restaurant_and_cat.restaurant_id AND restaurant.status = '.Restaurant::STATUS_ACTIVE.' WHERE cat_id IN ('.$filter_array.')')->queryAll();
        $result_restaurants = '';

        if (!empty($model_result)) {
            $input = array_map("unserialize", array_unique(array_map("serialize", $model_result)));

            foreach ($input as $restaurant) {
                if ($restaurant['id'] != null) {
                    $result_restaurants = $result_restaurants . '<div class="card col-md-3" style="margin-left: 10px; margin-top: 10px; margin-right: 10px;"><a href="site/restaurant?id='.$restaurant['id'].'"><img src="'.Url::base(true).$restaurant['main_photo'].'" style="height: 110px; width: 170px" alt=""><div class="description"><p>' . $restaurant['name'] . '<br></a><a href="site/restaurant?id='.$restaurant['id'].'&type=1" class="btn btn-danger btn-sm btn-block" style="position: center">ЗАБРОНИРОВАТЬ</a></p></div></div>';
                }
            }
        }

        return $result_restaurants;
    }

    public static function getMenuRestaurant($restaurant_id) {
        $model_result = RestaurantMenuDishes::find()->where(['restaurant_id' => $restaurant_id])->asArray()->all();
        $result_dishes = '';
        if (!empty($model_result)) {
            foreach ($model_result as $dishes) {
                $result_dishes = $result_dishes . '<option value="'.$dishes['id'].'">'.$dishes['name'].'</option>';
            }
        }

        return $result_dishes;
    }

    public static function getRestaurantAndCatById($restaurant_id) {
        $model_result = RestaurantAndCat::find()->select('restaurant_cat.name, restaurant_cat.id, restaurant_cat.parent_id')->leftJoin('restaurant_cat', 'restaurant_cat.id = restaurant_and_cat.cat_id')->where(['restaurant_id' => $restaurant_id])->asArray()->all();
        $parent_ids = [];

        if (!empty($model_result)) {
            foreach ($model_result as $parent_id) {
                array_push($parent_ids, $parent_id['parent_id']);
            }
;
            $model_parent = RestaurantCat::find()->where(['in', 'id', $parent_ids])->asArray()->all();
            return [
                'cat_restaurant' => $model_result,
                'parent' => $model_parent,
            ];
        }

        return $model_result;
    }

    public static function getRestaurantDishes($restaurant_id) {
        $model_dishes = RestaurantMenuDishes::find()->where(['restaurant_id' => $restaurant_id])->all();
        $result = '';

        if (!empty($model_dishes)) {
            foreach ($model_dishes as $dishes) {
                $result = $result.'<option value="'.$dishes->id.'" data-price="'.$dishes->price.'">'.$dishes->name. ' - ' . $dishes->price .'грн.</option>';
            }

            return $model_dishes;
        }

         return false;
    }

    public static function updateMainPhoto($file, $restaurant_id){
        $dir = Yii::getAlias('@image') ;
        $extension = $file->extension;
        $date_time = new DateTime();
        $current_time = $date_time->format('d_m_Y_H_i_s_u');
        $file_title = "file_{$current_time}.{$extension}";
        $file_path = Yii::getAlias('@file_path') . '/' . $file_title;

        if (file_exists($dir . $file_title)) {
            unlink($dir . $file_title);
        }

        if ($file->saveAs($dir . $file_title)) {
            $restaurant_update = Restaurant::find()->where(['id' => $restaurant_id])->one();
            $restaurant_update->main_photo = $file_path;
            $restaurant_update->save(false);
            return true;
        }
        return false;

    }

    public static function addNewGallery($file, $restaurant_id){
        $dir = Yii::getAlias('@image') ;
        $extension = $file->extension;
        $date_time = new DateTime();
        $current_time = $date_time->format('d_m_Y_H_i_s_u');
        $file_title = "file_{$current_time}.{$extension}";
        $file_path = Yii::getAlias('@file_path') . '/' . $file_title;

        if (file_exists($dir . $file_title)) {
            unlink($dir . $file_title);
        }

        if ($file->saveAs($dir . $file_title)) {
            $gallery_restaurant = new RestaurantGallery();
            $gallery_restaurant->restaurant_id = $restaurant_id;
            $gallery_restaurant->file_path = $file_path;
            $gallery_restaurant->save();
            return true;
        }
        return false;

    }

    public static function addGallery($model)
    {
        $gallery_array = [
            'initialPreview' => [],
            'showUpload' => false,
            'initialPreviewAsData' => true,
            'initialPreviewConfig' => [],
            'overwriteInitial' => false,
            'maxFileSize' => 100000
        ];
        if (!empty($model->gallerys)) {
            foreach ($model->gallerys as $image) {
                array_push($gallery_array,['deleteUrl' => Url::toRoute(['restaurant/delete-file', 'id' => $image->id])]);
                array_push($gallery_array['initialPreview'], $image['file_path']);
                array_push($gallery_array['initialPreviewConfig'], ['key' => $image['id']]);
            }
        }

        return $gallery_array;
    }


    //Method return discount price
    public static function discountPrice($user_id, $price) {
        $user_order_count = User::find()->where(['access' => USER::ROLE_USER, 'id' => $user_id])->count();

        if (!empty($user_order_count) && $user_order_count > 5) {
            $price_discount = $price / 100 * 35;
            return $price - $price_discount;
        }

        return $price;
    }

    /**
     * Method add user's cookies
     * @param string $name
     */
    public static function addUserCookiesByPostName($name) {
        if (!empty($name) && !empty(Yii::$app->request->post($name))) {
            self::addUserCookies($name, Yii::$app->request->post($name));
        }
    }

    public static function addUserCookies($name, $value) {
        if (!empty($name) && !empty($value)) {
            return Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => $name,
                'value' => $value
            ]));
        }
    }

}